# Copyright (C) 1998 Free Software Foundation, Inc.
# Sasha Konecni <sasha@msi-uk.com>, 1998.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase\n"
"POT-Creation-Date: 1999-04-08 01:20+0200\n"
"PO-Revision-Date: 1998-05-22 10:10+0000\n"
"Last-Translator: Sasha Konecni <sasha@msi-uk.com>\n"
"Language-Team: Macedonian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-5\n"
"Content-Transfer-Encoding: 8bit\n"

#: kdmshutdown.cpp:176
msgid "Shutdown or restart?"
msgstr "Prestani so rabota ili pochni povtorno?"

#: kdmshutdown.cpp:179
msgid "(Enter Root Password)"
msgstr ""

#: kdmshutdown.cpp:197
msgid "Shutdown"
msgstr "Prestani so rabota"

#: kdmshutdown.cpp:211
msgid "Shutdown and restart"
msgstr "Prestani so rabota i pochni povtorno"

#: kdmshutdown.cpp:220
msgid "Restart X Server"
msgstr ""

#: kgreeter.cpp:234
msgid "Login:"
msgstr "Login:"

#: kgreeter.cpp:246
msgid "Password:"
msgstr "Lozinka:"

#: kgreeter.cpp:271
msgid "Login failed!"
msgstr "Logiranjeto ne uspea!"

#: kgreeter.cpp:277
msgid "Session Type:"
msgstr "Vid na Sesija"

#: kgreeter.cpp:324
msgid "Go!"
msgstr "Odi!"

#: kgreeter.cpp:340
msgid "Shutdown..."
msgstr "Prestani so rabota..."

#: kgreeter.cpp:585
msgid "Logins not available right now."
msgstr ""

#: kgreeter.cpp:626
msgid "login_getcapstr() failed."
msgstr ""

#: kgreeter.cpp:680 kgreeter.cpp:707
msgid "Sorry -- your account has expired."
msgstr ""

#: kgreeter.cpp:685 kgreeter.cpp:712
#, c-format
msgid "Warning: your account expires on %s"
msgstr ""

#: kgreeter.cpp:739
msgid "Home directory not available"
msgstr ""

#~ msgid "Exit kdm"
#~ msgstr "Izlezi od kdm"
