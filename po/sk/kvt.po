# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kvt 0.18.1\n"
"POT-Creation-Date: 1999-03-04 18:41+0100\n"
"PO-Revision-Date: 1998-03-20 12:20 MET\n"
"Last-Translator: Juraj Bedn�r <bednar@isternet.sk>\n"
"Language-Team: Slovak <bednar@isternet.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#: main.C:426
msgid "choose type of color-mode"
msgstr "Typ farebn�ho m�du:"

#: main.C:432
msgid "Add characters to word class"
msgstr "Znaky pridan� do triedy slov:"

# ???
#: main.C:438
msgid "The backspace key will send a"
msgstr "Backspace bude posielan� ako"

#: main.C:444
msgid "Cursor key set"
msgstr "Sada kurzorov�ch kl�ves"

#: main.C:613
msgid "&New Terminal"
msgstr "&Nov� termin�l"

#: main.C:620 main.C:1043
msgid "&Hide"
msgstr "&Schova�"

#: main.C:621
msgid "&Left"
msgstr "N&a�avo"

#: main.C:622
msgid "&Right"
msgstr "Nap&ravo"

#: main.C:627
msgid "&Normal"
msgstr "&Norm�lne"

#: main.C:628
msgid "&Tiny"
msgstr "&Drobn�"

#: main.C:629
msgid "&Small"
msgstr "&Mal�"

#: main.C:630
msgid "&Medium"
msgstr "&Stredn�"

#: main.C:631
msgid "&Large"
msgstr "V&e�k�"

#: main.C:632
msgid "&Huge"
msgstr "&Obrovsk�"

#: main.C:644
msgid "&black/white"
msgstr "�ierna/&biela"

#: main.C:645
msgid "&white/black"
msgstr "b&iela/�ierna"

#: main.C:646
msgid "&green/black"
msgstr "zelen�/�ie&rna"

#: main.C:647
msgid "black/light&yellow"
msgstr "�ierna/svetlo �l&t�"

#: main.C:648
msgid "Linu&x Console"
msgstr "Linu&x konzola"

#: main.C:657
msgid "&Custom Colors"
msgstr "&Vlastn� farby"

#: main.C:665 main.C:969
msgid "Hide &Menubar"
msgstr "Skry� &menu"

#: main.C:667 main.C:888 main.C:960
msgid "Show &Menubar"
msgstr "Zobrazi� &menu"

#: main.C:668 main.C:979
msgid "Secure &keyboard"
msgstr "Zabezpe�i� &kl�vesnicu"

#: main.C:670
msgid "Scroll&bar"
msgstr "R&olovacia li�ta"

#: main.C:671
msgid "&Font size"
msgstr "Ve�kos� &p�sma"

#: main.C:672
msgid "&Color"
msgstr "F&arba"

#: main.C:673
msgid "&Size"
msgstr "Ve�ko&s�"

#: main.C:674
msgid "Terminal..."
msgstr "&Termin�l..."

#: main.C:675
msgid "Font..."
msgstr "P�smo..."

#: main.C:676
msgid "Enable &hotkeys"
msgstr "Povoli� &hor�ce kl�vesy"

#: main.C:679
msgid "Save &Options"
msgstr "Ulo�i� &nastavenie"

#: main.C:688
msgid ""
"\n"
"\n"
"(C) 1996, 1997 Matthias Ettrich (ettrich@kde.org)\n"
"(C) 1997 M G Berberich (berberic@fmi.uni-passau.de)\n"
"(C) 1998 Ivan Schreter (schreter@kdk.sk)\n"
"(C) 1999 Leon Widdershoven (l.widdershoven@fz-juelich.de)\n"
"\n"
"Terminal emulation for the KDE Desktop Environment\n"
"based on Robert Nation's rxvt-2.08"
msgstr ""
"\n"
"\n"
"(C) 1996, 1997 Matthias Ettrich (ettrich@kde.org)\n"
"(C) 1997 M G Berberich (berberic@fmi.uni-passau.de)\n"
"(C) 1998 Ivan Schreter (schreter@kdk.sk)\n"
"(C) 1999 Leon Widdershoven (l.widdershoven@fz-juelich.de)\n"
"\n"
"Emul�cia termin�lu pre KDE Desktop Environment\n"
"zalo�en� na  rxvt-2.08 Roberta Nationa"

#: main.C:897 main.C:1036
msgid "&Show"
msgstr "&Uk�za�"

#: main.C:975
msgid "Unsecure &keyboard"
msgstr "Vypn�� zabezpe�enie &kl�vesnice"

#: main.C:986
msgid "Terminal Options"
msgstr "Nastavenie termin�lu"

#: main.C:1152
msgid "Hint"
msgstr "Rada"

#: main.C:1153
msgid ""
"New color settings will be displayed\n"
"when the keyboard is unsecured."
msgstr ""
"Nov� nastavenie farieb sa prejav�\n"
"po vypnut� zabezpe�enia kl�vesnice."

#: main.C:1474
msgid ""
"Copyright(C) 1996, 1997 Matthias Ettrich\n"
"Copyright(C) 1997 M G Berberich\n"
"Terminal emulation for the KDE Desktop Environment\n"
"based on Robert Nation's rxvt-2.08\n"
"\n"
msgstr ""
"(C) 1996, 1997 Matthias Ettrich (ettrich@kde.org)\n"
"(C) 1997 M G Berberich (berberic@fmi.uni-passau.de)\n"
"Emul�cia termin�lu pre KDE Desktop Environment\n"
"Zalo�en� na  rxvt-2.08 Roberta Nationa\n"
"\n"

#: main.C:1478
msgid "Permitted arguments are:\n"
msgstr "Povolen� argumenty s�:\n"

#: main.C:1479
msgid ""
"-e <command> <arg> ...    execute command with ars - must be last argument\n"
msgstr ""
"-e <pr�kaz> <arg> ...     prevedie pr�kaz s parametrami - mus� by� posledn�\n"
"                          argument\n"

#: main.C:1480
msgid "-display <name>           specify the display (server)\n"
msgstr "-display <meno>           �pecifikuje display (server)\n"

#: main.C:1481
msgid "-vt_geometry <spec>       the initial vt window geometry\n"
msgstr "-vt_geometry <spec>       po�iato�n� geometria vt okna\n"

#: main.C:1482
msgid "-print-pipe <name>        specify pipe for vt100 printer\n"
msgstr "-print-pipe <n�zov>       �pecifikuje r�ru pre vt100 tla�iare�\n"

#: main.C:1483
msgid "-vt_bg <colour>           background color\n"
msgstr "-vt_bg <farba>            farba pozadia\n"

#: main.C:1484
msgid "-vt_fg <colour>           foreground color\n"
msgstr "-vt_fg <farba>            farba popredia\n"

#: main.C:1485
msgid "-vt_font <fontname>       normal font\n"
msgstr "-vt_font <n�zov>          norm�lny font\n"

#: main.C:1486
msgid "-no_font_hack             turn off Qt font bug workaround\n"
msgstr "-no_font_hack             vypn�� ob�denie chyby Qt fontu\n"

#: main.C:1487
msgid "-vt_size <size>           tiny, small, normal, large, huge\n"
msgstr "-vt_size <ve�kos�>        tiny, small, normal large, huge\n"

#: main.C:1488
msgid "-linux                    set up kvt to mimic linux-console\n"
msgstr "-linux                    nastav� kvt na emul�ciu linux�ckej konzoly\n"

#: main.C:1489
msgid "-no_menubar               hide the menubar\n"
msgstr "-no_menubar               skryje riadok s menu\n"

#: main.C:1490
msgid "-no_scrollbar             hide the scrollbar\n"
msgstr "-no_scrollbar             skryje rolovac� p�s\n"

#: main.C:1491
msgid ""
"-T <text>                 text in window titlebar \n"
"                          (Obsolete. Use -caption instead)\n"
msgstr ""
"-T <text>                 text v titulku okna \n"
"                          (Zastaral�. Pou�ite rad�ej -caption)\n"

#: main.C:1493
msgid "-tn <TERM>                Terminal type. Default is xterm.\n"
msgstr "-tn <TERM>                Typ termin�lu. �tandardne xterm.\n"

#: main.C:1494
msgid "-C                        Capture system console message\n"
msgstr "-C                        Odchyt�va spr�vy syst�movej konzoly\n"

#: main.C:1495
msgid "-n <text>                 name in icon or icon window\n"
msgstr "-n <text>                 n�zov v ikone alebo okne ik�n\n"

#: main.C:1496
msgid ""
"-7                        run in 7 bit mode\n"
"-8                        run in 8 bit mode\n"
msgstr ""
"-7                        spusti� v 7-bitovom m�de\n"
"-8                        spusti� v 8-bitovom m�de\n"

#: main.C:1498
msgid ""
"-ls                       initiate kvt's shell as a login shell\n"
"-ls-                      initiate kvt's shell as a non-login shell "
"(default)\n"
msgstr ""
"-ls                       spusti� shell kvt ako prihlasovac� shell\n"
"-ls-                      nespusti� shell kvt ako prihlasovac� shell "
"(�tand.)\n"

#: main.C:1500
msgid ""
"-meta <arg>               handle Meta with ESCAPE prefix, 8THBIT set, or "
"ignore\n"
msgstr ""
"-meta <arg>               spracova� Meta s prefixom ESCAPE, nastaven�m 8. "
"bitom\n"
"                          alebo ignorova�\n"

#: main.C:1501
msgid "-sl <number>              save number lines in scroll-back buffer\n"
msgstr ""
"-sl <��slo>               ulo�i� '��slo' riadkov do buffra scrollbaru\n"

#: main.C:1502
msgid ""
"-pageup <keysym>          use hot key alt-keysym to scroll up through the "
"buffer\n"
"-pagedown <keysym>        use hot key alt-keysym to scroll down through "
"buffer\n"
msgstr ""
"-pageup <keysym>          hotkey alt-keysym na prech�dzanie hore bufferom\n"
"-pagedown <keysym>        hotkey alt-keysym na prech�dzanie dole bufferom\n"

#: main.C:1506
msgid ""
"-grk9\t\tgreek kbd = ELOT 928 (default)\n"
"-grk4\t\tgreek kbd = IBM 437\n"
msgstr ""
"-grk9\\t\\tgreek kbd = ELOT 928 (�tandardn�)\n"
"-grk4\\t\\tgreek kbd = IBM 437\n"
