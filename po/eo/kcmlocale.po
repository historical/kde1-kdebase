# Esperantaj mesa�oj por "kcmlocale"
# Copyright (C) 1998 Free Software Foundation, Inc.
# Wolfram Diestel <diestel@rzaix340.rz.uni-leipzig.de>, 1998.
#
#
msgid ""
msgstr ""
"Project-Id-Version: kcmlocale 1.0pre2\n"
"POT-Creation-Date: 1998-05-13 12:05+0200\n"
"PO-Revision-Date: 1998-07-15 20:00+0100\n"
"Last-Translator: Wolfram Diestel <diestel@rzaix340.rz-uni-leipzig.de>\n"
"Language-Team: Esperanto <eo@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-3\n"
"Content-Transfer-Encoding: 8-bit\n"

#: main.cpp:52
msgid "&Locale"
msgstr "&Loka�o"

#: main.cpp:58
msgid "usage: kcmlocale [-init | language]\n"
msgstr "uzo: kcmlocale [-init | language]\n"

#: main.cpp:86
msgid "Locale settings"
msgstr "Loka�oj"

#: locale.cpp:45
msgid "Language"
msgstr "Lingvo"

#: locale.cpp:55
msgid "First"
msgstr "Unua"

#: locale.cpp:65
msgid "Second"
msgstr "Dua"

#: locale.cpp:75
msgid "Third"
msgstr "Tria"

#: locale.cpp:115
msgid "without name!"
msgstr "sen nomo!"

#: locale.cpp:181
msgid "Applying language settings"
msgstr "Apliko de la lingvoagorda�o"

#: locale.cpp:182
msgid ""
"Changed language settings apply only to newly started applications.\n"
"To change the language of all programs, you will have to logout first."
msgstr ""
"La �an�itaj lingvoj apliki�as nur al lan�otaj aplika�oj.\n"
"Por �an�i la lingvon de �iuj programoj, vi devas unue adia�i."
