# Copyright (C) 1998 Free Software Foundation, Inc.
# Vladimir Vuksan <vuksan@veus.hr>, 1998
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase\n"
"POT-Creation-Date: 1999-01-27 01:14+0100\n"
"PO-Revision-Date: 1998-10-15 15:00-0500\n"
"Last-Translator: Vladimir Vuksan <vuksan@veus.hr>\n"
"Language-Team: Croatian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#: configlist.cpp:121
msgid "Unknown module: "
msgstr "Nepoznati modul: "

#: main.cpp:46 mainwidget.cpp:19
msgid "KDE Control Center"
msgstr "KDE kontrolni centar"

#: toplevel.cpp:86
msgid "&Swallow modules"
msgstr "&Progutaj module"

#: toplevel.cpp:89
msgid ""
"KDE Control Center - Version 1.0\n"
"\n"
"Written by Matthias H�lzer\n"
"(hoelzer@physik.uni-wuerzburg.de)\n"
"\n"
"Thanks to:\n"
"S. Kulow, P. Dowler, M. Wuebben & M. Jones."
msgstr ""
"KDE Control Center - ina�ica 1.0\n"
"\n"
"Napisan od strane Matthias H�lzer-a\n"
"(hoelzer@physik.uni-wuerzburg.de)\n"
"\n"
"Zahvale:\n"
"S. Kulow, P. Dowler, M. Wuebben & M. Jones."

#: mainwidget.cpp:44
msgid "KDE Version: "
msgstr "KDE Ina�ica: "

#: mainwidget.cpp:52
msgid "User: "
msgstr "Korisnik: "

#: mainwidget.cpp:58
msgid "Hostname: "
msgstr "Ime ra�unala: "

#: mainwidget.cpp:66
msgid "System: "
msgstr "Sistem: "

#: mainwidget.cpp:73
msgid "Release: "
msgstr "Izdanje: "

#: mainwidget.cpp:81
msgid "Version: "
msgstr "Ina�ica: "

#: mainwidget.cpp:88
msgid "Machine: "
msgstr "Stroj: "
