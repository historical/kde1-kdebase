msgid ""
msgstr ""
"Project-Id-Version: kuser\n"
"POT-Creation-Date: 1999-01-27 01:14+0100\n"
"PO-Revision-Date: 1998-03-23 00:00+0100\n"
"Last-Translator: Sandro Henrique <sandro@conectiva.com.br>\n"

#: configlist.cpp:121
msgid "Unknown module: "
msgstr "M�dulo desconhecido: "

#: main.cpp:46 mainwidget.cpp:19
msgid "KDE Control Center"
msgstr "Centro de Controle KDE"

#: toplevel.cpp:86
msgid "&Swallow modules"
msgstr "Ab&sorver m�dulos"

#: toplevel.cpp:89
msgid ""
"KDE Control Center - Version 1.0\n"
"\n"
"Written by Matthias H�lzer\n"
"(hoelzer@physik.uni-wuerzburg.de)\n"
"\n"
"Thanks to:\n"
"S. Kulow, P. Dowler, M. Wuebben & M. Jones."
msgstr ""
"Centro de Controle KDE - Vers�o 1.0\n"
"\n"
"Escrito por Matthias H�lzer\n"
"(hoelzer@physik.uni-wuerzburg.de)\n"
"\n"
"Agradecimentos a:\n"
"S. Kulow, P. Dowler, M. Wuebben & M. Jones."

#: mainwidget.cpp:44
msgid "KDE Version: "
msgstr "Vers�o do KDE: "

#: mainwidget.cpp:52
msgid "User: "
msgstr "Usu�rio:"

#: mainwidget.cpp:58
msgid "Hostname: "
msgstr "Nome de host: "

#: mainwidget.cpp:66
msgid "System: "
msgstr "Sistema: "

#: mainwidget.cpp:73
msgid "Release: "
msgstr "Release: "

#: mainwidget.cpp:81
msgid "Version: "
msgstr "Vers�o: "

#: mainwidget.cpp:88
msgid "Machine: "
msgstr "M�quina: "

#~ msgid "System:"
#~ msgstr "Sistema:"
